package main;

import calcul.CalculPath;
import terrain.Terrain;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Terrain terrain = new Terrain(3);
		System.out.println("Le d�but est en " + (terrain.getDebut().getX() + 1) + " " + (terrain.getDebut().getY() + 1));
		System.out.println("La fin est en " + (terrain.getFin().getX() + 1) + " " + (terrain.getFin().getY() + 1));
		System.out.println();
		System.out.println(terrain.getLengthBetweenPoints(terrain.getDebut(), terrain.getFin()));
		System.out.println();
		System.out.println();
		System.out.println();
		CalculPath.calcul(terrain);
	}

}
