package caseDeJeu;

public class Case {

	private boolean isDebut;
	private boolean isFin;
	private boolean isSnake;
	private int x;
	private int y;
	
	public Case(int x, int y) {
		this.setDebut(false);
		this.setFin(false);
		this.setSnake(false);
		this.setX(x);
		this.setY(y);
	}
	
	public boolean isDebut() {
		return isDebut;
	}
	
	public void setDebut(boolean isDebut) {
		this.isDebut = isDebut;
	}
	
	public boolean isFin() {
		return isFin;
	}
	
	public void setFin(boolean isFin) {
		this.isFin = isFin;
	}
	
	public boolean isSnake() {
		return isSnake;
	}
	
	public void setSnake(boolean isSnake) {
		this.isSnake = isSnake;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
}
