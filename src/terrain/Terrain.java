package terrain;

import caseDeJeu.Case;

public class Terrain {

	private Case[][] cases;
	private int size;
	
	public Terrain(int size) {
		this.setSize(size);
		this.setCases(new Case[size][size]);
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				this.getCases()[i][j] = new Case(i, j);
			}
		}
		this.setRandomDebutAndFin();
		//this.setSnake();
	}
	
	public void afficherTerrain() {
		for(int i = 0; i < this.getSize(); i++) {
			for(int j = 0; j < this.getSize(); j++) {
				System.out.print(this.getCases()[i][j].isSnake() ? 'x' : '.');
				System.out.print(" ");
				if(j == this.getSize() - 1) {
					System.out.println(this.getNumberOfSnakeCasesOnRow(i));
				}
			}
		}
		for(int i = 0; i < this.getSize(); i++) {
			System.out.print(this.getNumberOfSnakeCasesOnColumn(i) + " ");
		}
	}
	
	public void setSnake() {
		//TEMPORAIRE
		this.getCases()[0][4].setDebut(true);
		this.getCases()[0][4].setSnake(true);
		this.getCases()[1][4].setSnake(true);
		this.getCases()[2][4].setSnake(true);
		this.getCases()[2][3].setSnake(true);
		this.getCases()[2][2].setSnake(true);
		this.getCases()[2][1].setSnake(true);
		this.getCases()[2][0].setSnake(true);
		this.getCases()[2][0].setFin(true);
		
	}
	
	public int getNumberOfSnakeCasesOnRow(int rowNumber) {
		if(rowNumber > this.getSize() - 1) {
			return 0;
		}
		int cpt = 0;
		for(int i = 0; i < this.getSize(); i++) {
			if(this.getCases()[rowNumber][i].isSnake()) {
				cpt++;
			}
		}
		return cpt;
	}
	
	public int getNumberOfSnakeCasesOnColumn(int columnNumber) {
		if(columnNumber > this.getSize() - 1) {
			return 0;
		}
		int cpt = 0;
		for(int i = 0; i < this.getSize(); i++) {
			if(this.getCases()[i][columnNumber].isSnake()) {
				cpt++;
			}
		}
		return cpt;
	}
	
	public void setRandomDebutAndFin() {
		int caseUp = (int) (Math.random() * this.getSize());
		int caseSide;
		do {
			caseSide = (int) (Math.random() * this.getSize());
		} while(caseSide == caseUp);
		
		
		this.getCases()[0][caseUp].setDebut(true);
		this.getCases()[caseSide][0].setFin(true);
		
		this.getCases()[0][caseUp].setSnake(true);
		this.getCases()[caseSide][0].setSnake(true);
	}
	
	public double getLengthBetweenPoints(Case caseA, Case caseB) {
		//Application du th�or�me de Thal�s
		double sideA = (caseA.getX() + caseA.getY()) * (caseA.getX() + caseA.getY());
		double sideB = (caseB.getX() + caseB.getY()) * (caseB.getX() + caseB.getY());
		double lengthSquare = sideA + sideB;
		return Math.sqrt(lengthSquare);
	}
	
	public Case getDebut() {
		for(int i = 0; i < this.getSize(); i++) {
			for(int j = 0; j < this.getSize(); j++) {
				if(this.getCases()[i][j].isDebut()) {
					return this.getCases()[i][j];
				}
			}
		}
		return null;
	}
	
	public Case getFin() {
		for(int i = 0; i < this.getSize(); i++) {
			for(int j = 0; j < this.getSize(); j++) {
				if(this.getCases()[i][j].isFin()) {
					return this.getCases()[i][j];
				}
			}
		}
		return null;
	}
	
	public Case[][] getCases() {
		return cases;
	}

	public void setCases(Case[][] cases) {
		this.cases = cases;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
}
