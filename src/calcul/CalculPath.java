package calcul;

import terrain.Terrain;

public class CalculPath {

	public static void calcul(Terrain terrain) {
		int n = terrain.getSize() * terrain.getSize();
		
		int[][] matriceAdjacence = new int[n][n];
		
		// initialisation
		for(int i = 0; i < terrain.getSize(); i++) {
			for(int j = 0; j < terrain.getSize(); j++) {
				matriceAdjacence[i][j] = 0;
			}
		}
		
		int source = 0;
		int target = 0;
		
		int cpt = 0;
		for(int i = 0; i < terrain.getSize(); i++) {
			for(int j = 0; j < terrain.getSize(); j++) {
				// BAS
				// matriceAdjacence[(i)*(j)][cpt + 3] = 1;
				// HAUT
				// matriceAdjacence[(i)*(j)][cpt - 3] = 1;
				// DROITE
				// matriceAdjacence[(i)*(j)][cpt + 1] = 1;
				// GAUCHE
				// matriceAdjacence[(i)*(j)][cpt -1] = 1;
				if(j != 0 && i != 0 && j != terrain.getSize() - 1 && i != terrain.getSize() - 1) {
					// Si ce n'est pas un coin ou ligne de bord alors on met les 4 cases autour
					matriceAdjacence[cpt][cpt + 3] = 1;
					matriceAdjacence[cpt][cpt - 3] = 1;
					matriceAdjacence[cpt][cpt + 1] = 1;
					matriceAdjacence[cpt][cpt - 1] = 1;
				} else if(j == 0 && i == 0) {
					// Case en haut � gauche
					matriceAdjacence[cpt][cpt + 1] = 1;
					matriceAdjacence[cpt][cpt + 3] = 1;
				} else if(j == 0 && i == terrain.getSize() - 1) {
					// Case en bas � gauche
					matriceAdjacence[cpt][cpt + 1] = 1;
					matriceAdjacence[cpt][cpt - 3] = 1;
				} else if(j == terrain.getSize() - 1 && i == terrain.getSize() - 1) {
					// Case en bas � droite
					matriceAdjacence[cpt][cpt - 1] = 1;
					matriceAdjacence[cpt][cpt - 3] = 1;
				} else if(j == terrain.getSize() - 1 && i == 0) {
					// Case en haut � droite
					matriceAdjacence[cpt][cpt - 1] = 1;
					matriceAdjacence[cpt][cpt + 3] = 1;
				} else if(i == 0) {
					// Cases de la ligne haute
					matriceAdjacence[cpt][cpt + 3] = 1;
					matriceAdjacence[cpt][cpt - 1] = 1;
					matriceAdjacence[cpt][cpt + 1] = 1;
				} else if(i == terrain.getSize() - 1) {
					// Cases de la ligne basse
					matriceAdjacence[cpt][cpt - 3] = 1;
					matriceAdjacence[cpt][cpt - 1] = 1;
					matriceAdjacence[cpt][cpt + 1] = 1;
				} else if(j == 0) {
					// Cases de la ligne gauche
					matriceAdjacence[cpt][cpt - 3] = 1;
					matriceAdjacence[cpt][cpt + 1] = 1;
					matriceAdjacence[cpt][cpt + 3] = 1;
				} else if(j == terrain.getSize() - 1) {
					// Cases de la ligne de droite
					matriceAdjacence[cpt][cpt - 3] = 1;
					matriceAdjacence[cpt][cpt - 1] = 1;
					matriceAdjacence[cpt][cpt + 3] = 1;
				}
				
				if(i == terrain.getDebut().getX() && j == terrain.getDebut().getY()) {
					source = cpt;
				}
				if(i == terrain.getFin().getX() && j == terrain.getFin().getY()) {
					target = cpt;
				}
				cpt++;
			}
		}
		
		int[] path = new int[n];
		boolean[] taboo = new boolean[n];
		
		boolean fini = false;
		int depth = 0;
		
		while(!fini) {
			path[depth] = source;

			if(source == target) {
				for(int i = 0; i <= depth; i++) {
					System.out.print(path[i] + " ");
				}
				System.out.println();
				fini = true;
			} else {
				taboo[source] = true;
				
				for(int i = 0; i < n; i++) {
					if(matriceAdjacence[source][i] == 0 || taboo[i]) {
						continue;
					} else {
						source = i;
						depth++;
					}
				}
			}
		}
	}
}
